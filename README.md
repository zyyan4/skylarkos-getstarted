# SkylarkOS Get Started
SkylarkOS是基于NationalChip AI芯片构建的嵌入式Linux系统，集成了语音信号处理，神经网络运算，WIFI，蓝牙，播放器，GUI等丰富的功能组件，内置灵活高效的JS APP框架，帮助用户快捷地打造自己的AI应用方案

#### 框架概览:
![SkylarkOS.png](SkylarkOS.png  "SkylarkOS框架")

***

## 安装编译依赖工具
### Ubuntu16.04
```
sudo apt-get install build-essential subversion libncurses5-dev zlib1g-dev gawk gcc-multilib flex git-core gettext libssl-dev unzip texinfo device-tree-compiler dosfstools libusb-1.0-0-dev
```
### CentOS7
```
yum install -y unzip bzip2 dosfstools wget gcc gcc-c++ git ncurses-devel zlib-static openssl-devel svn patch perl-Module-Install.noarch perl-Thread-Queue
```
CentOS7需自行安装device-tree-compiler：
```
wget http://www.rpmfind.net/linux/epel/6/x86_64/Packages/d/dtc-1.4.0-1.el6.x86_64.rpm
rpm -i dtc-1.4.0-1.el6.x86_64.rpm
```
***

## 获取源码
拉取以下项目代码，并放到同一目录下:

-  [openwrt](https://gitlab.com/nationalchip/openwrt)
-  [kernel](https://gitlab.com/nationalchip/kernel)
-  [uboot](https://gitlab.com/nationalchip/uboot)
-  [middleware](https://gitlab.com/nationalchip/middleware)
-  [skylark](https://gitlab.com/nationalchip/skylark)

***

## 编译

1.  进入openwrt目录
```
cd openwrt
```
2.  拷贝方案defconfig配置到.config, 例如:
```
cp configs/leo_gx8009b_ssd_lc_v1_skylark_defconfig .config
```
3.  生成.config配置文件
```
make defconfig
```
4.  make
```
make
```
- 如果想加快编译速度，请加`-j`参数
- 如果想查看详细编译信息，请加`V=s`参数

***

## 烧写固件
编译生成的固件存放在openwrt/bin/<方案>/ 目录, 可使用download.sh脚本，配合板子上的boot键来烧写：
```
usage:
 download.sh <mcu | uboot | kernel | rootfs | all | clean>
    mcu:    download mcu
    uboot:  download uboot stage1 and uboot stage2
    kernel: download kernel dtb and its Image
    rootfs: download rootfs
    ota:    download ota
    all:    download all bin
    clean:  clean all flash
```
***

## 文档
- [开发指南](https://ai.nationalchip.com/docs/gx8010/)

***

## 当前版本
- [V1.4.0-rc1](release_note/RN_1_4_0_RC1.md)
